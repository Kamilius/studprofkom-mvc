ALTER DATABASE StudProfkomUAD
SET COMPATIBILITY_LEVEL = 100;

USE StudProfkomUAD;

ALTER TABLE NewsArticle
ALTER COLUMN Title NVARCHAR(MAX);

/*Publication date to BIGINT*/
EXEC sp_rename 'NewsArticle.PublicationDate', 'PublicationDateOld', 'COLUMN';

ALTER TABLE NewsArticle
ADD PublicationDate BIGINT;

UPDATE NewsArticle 
SET PublicationDate = datediff(SECOND,'1970-1-1', na.PublicationDateOld)
FROM NewsArticle
JOIN NewsArticle na ON NewsArticle.Id = na.Id;

ALTER TABLE NewsArticle
ALTER COLUMN PublicationDate BIGINT NOT NULL;

ALTER TABLE NewsArticle
DROP COLUMN PublicationDateOld;

/*Event start date to BIGINT*/

EXEC sp_rename 'NewsArticle.EventStartDate', 'EventStartDateOld', 'COLUMN';

ALTER TABLE NewsArticle
ADD EventStartDate BIGINT;

UPDATE NewsArticle 
SET EventStartDate = datediff(SECOND,'1970-1-1', na.EventStartDateOld)
FROM NewsArticle
JOIN NewsArticle na ON NewsArticle.Id = na.Id;

ALTER TABLE NewsArticle
DROP COLUMN EventStartDateOld;

/*EventShortDescription size change 50 -> 255*/
ALTER TABLE NewsArticle
ALTER COLUMN EventShortDescription NVARCHAR(255);

ALTER TABLE NewsArticle
ALTER COLUMN ArticleShortDescription NVARCHAR(MAX);

ALTER TABLE NewsArticle
ALTER COLUMN ImageName NVARCHAR(255);

UPDATE NewsArticle SET ImageName = cast(ImageName as nvarchar(MAX)) + cast('.jpg' as nvarchar(MAX));

ALTER TABLE NewsArticle
DROP COLUMN ImageUrl;


