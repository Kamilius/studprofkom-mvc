USE StudProfkomUAD;
GO

CREATE TABLE Faculty 
(
	Id int identity (1, 1) primary key,
	Name nvarchar(250)
);
GO

INSERT INTO Faculty (Name)
VALUES ('����'), ('�ϲ�'), ('�ϲ');

ALTER TABLE Application
  ADD City nvarchar(250);
GO

update Application
set City = '�� �������'
where City IS NULL

EXEC sp_rename 'Application.Faculty', 'Faculty_FK', 'COLUMN';
GO

update Application
set Faculty_FK = '1'
where Faculty_FK = '����' OR Faculty_FK = '�����';

update Application
set Faculty_FK = '2'
where Faculty_FK = '�ϲ�' OR Faculty_FK = '��ϲ�';

update Application
set Faculty_FK = '2'
where Faculty_FK = '�ϲ' OR Faculty_FK = '��ϲ';

alter table Application 
alter column Faculty_FK int;
GO

--for changing table owner to a proper one
--ALTER SCHEMA dbo TRANSFER [stuprof].Faculty;