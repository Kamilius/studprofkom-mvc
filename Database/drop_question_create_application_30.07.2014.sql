DROP TABLE QuestionAnswer;

CREATE TABLE Application 
(
	Id int identity (1, 1) primary key,
	FullName nvarchar(250),
	Faculty nvarchar(250),
	Phone nvarchar(25),
	CreateDate datetime
)
